Budget
========================================
Budget is a set of scripts developed by Elioth (https://elioth.com/) to create data tables describing :
- Share of the available income of a household spent on a list of consumption items (INSEE BdF database).
- Available income per consumption unit, given the socio-economic characteristics of a household (Filosofi and Recensement database).

## Installation
```
conda install pandas numpy scipy pyarrow
pip install ipfn
```

## Use
To compute the probability of available income levels per consumption unit, for a given household (age of the reference person, type of household and tenant/owner status), run the income_probability.py script :
```
python income_probability.py
```
Its output is the income_probability.parquet file in ./python/data/output/income_probability.parquet.

The ./python/data/input/age_household_type_occtypr_filosofi.csv input file can be generated with the Population module.

## Licence
Copyright elioth

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.

You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
