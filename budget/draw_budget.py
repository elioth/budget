# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 14:10:46 2020

@author: l.gontier
"""

from pathlib import Path
import pandas as pd
import numpy as np
import pyarrow.parquet as pq
import os


def hh_budget_bdf(dept, hh_data, replicable=None, known_budgets = None):
    """
    
    Parameters
    ----------
    dept : str
        Department number.
    hh_data : DataFrame
        Households data.
    housing : boolean
        indicates if LCA housing data has been given or not
    mobility : boolean
        indicates if LCA mobility data has been given or not

    Returns
    -------
    hh_data : DataFrame
        Households data with budget and decile.
    total : DataFrame
        Budget repartition for each household in BdF classification.

    """
    
    data_folder_path = Path(os.path.dirname(__file__))
    data_folder_path = data_folder_path.parent / "data"
    
    if known_budgets is not None:
        hh_data["budget"] = known_budgets
    else:
        # Budget of the households is drawn thanks to income probability depending on department
        with open(data_folder_path / "output/income_probability.parquet", 'rb') as f:
            income_p = pd.read_parquet(f, engine='pyarrow')
        income_p = income_p.loc[income_p['DEPT']==dept]
        prob = pd.merge(hh_data, income_p[['household_type_filosofi', 'age_filosofi', 'occupation_status', 'income', 'p']].rename(columns={'age_filosofi':'ref_pers_age_filosofi', 'occupation_status':'occupation_status_filosofi'}), on=['household_type_filosofi', 'ref_pers_age_filosofi', 'occupation_status_filosofi'], how='left')
        prob[['income', 'p']]=prob[['income', 'p']].astype(float)
        
        hh_data['budget'] = 0
        # print(hh_data)
        seed = 1810
        for i in hh_data['household_id']:
            p = np.array(prob.loc[prob['household_id']==i, "p"])
            p /= p.sum()  # normalize
            
            # if order is None:
    
            #     hh_data.loc[hh_data['household_id']==i, "budget"] = np.random.choice(prob.loc[prob['household_id']==i, "income"], p=p)
            # else:
            #     p_max = prob.loc[prob['household_id']==i, "p"].max()
                
            #     hh_data.loc[hh_data['household_id']==i, "budget"] = prob.loc[(prob['household_id']==i)&(prob["p"]==p_max), "income"].values[0]
            if (replicable): #EP
                # Replicate (always start with the same seed) but randomize choice (update seed each time)
                r = np.random.RandomState(seed)
                hh_data.loc[hh_data['household_id'] == i, "budget"] = r.choice(prob.loc[prob['household_id'] == i, "income"], p=p)
                seed +=10 #change seed to change the next time choice
            else:
                hh_data.loc[hh_data['household_id'] == i, "budget"] = np.random.choice(prob.loc[prob['household_id'] == i, "income"], p=p) 
                         
    
    
        # Convert to actualised value of euro : 1€ in 2017 is equivalent to 0.96€ in 2011
        hh_data['budget'] = hh_data['budget']*0.96 # Value of budget in 2011€ value
    
    # Link housholds budgets to the french standard of living category (in deciles)
    france = pd.read_excel(data_folder_path / 'input/insee/budget/decile_France_2011.xlsx') # French deciles of living standards
    hh_data['decile_FR'] = '0'
    for i in range(len(hh_data.household_id)):
        hh_data.loc[i, 'decile_FR']= france.loc[(france.lim_h>=hh_data.budget[i]) & (france.lim_b<hh_data.budget[i]), 'decile'].values[0] # LG : hh_data['decile_FR'][i]
    
    # Compute the budget share of the household 
    # Consommation distribution for a household given its type, the reference person age and the available income per consumption unit
    with open(data_folder_path / 'output/budget.parquet', 'rb') as f:
        conso = pd.read_parquet(f, engine='pyarrow')
    conso = conso.rename(columns={'decile':'decile_FR', 'age':'ref_pers_age', 'stocd': 'occupation_status'})
    total = pd.merge(hh_data, conso, on = ['decile_FR', 'household_type', 'ref_pers_age', 'occupation_status'], how = 'left') 
    
    # Add a quality factor 
    ecart = pd.read_csv(data_folder_path / 'input/elioth/ecart.csv')
    ecart= pd.melt(ecart, id_vars = ['budget_item_id', 'ratio'], var_name='decile_FR')
    total = pd.merge(total, ecart, on=['budget_item_id', 'decile_FR'], how='left')
    total['budget_item']= total['budget']*total['share']*total['ratio']/total['value']
    total = total.drop(columns=['ratio', 'value'])
    
    
    return hh_data, total


