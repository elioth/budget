import pandas as pd
from ipfn import ipfn
import numpy as np
import sparse
from pathlib import Path
import os

def main():

    data_folder_path = Path(os.path.abspath(__file__))
    data_folder_path = data_folder_path.parent / "data"

    # --------------------------------
    # Load household budget given the type of household
    budget_type = pd.read_excel(
        data_folder_path / "input/insee/budget/irsocbdf11_TM105.xls",
        skiprows=2,
        usecols=[0, 2, 3, 4, 5, 6]
    )

    budget_type.columns = [
        "budget_item_id",
        "Personnes seules",
        "Familles monoparentales",
        "Couples sans enfant",
        "Couples avec enfants",
        "Autres ménages"
    ]

    # Filter out grouped items
    budget_type["budget_item_id"] = budget_type["budget_item_id"].str.split(" - ", 0).str.get(0)
    budget_type = budget_type[budget_type["budget_item_id"].str.len() == 5]

    # Melt dataframe
    budget_type = pd.melt(budget_type, id_vars="budget_item_id", var_name="household_type")

    # Households number
    n_households = pd.read_excel(
        data_folder_path / "input/insee/budget/irsocbdf11_cad01_fm.xls",
        skiprows=19,
        nrows=1,
        usecols=[1, 2, 3, 4, 5]
    )

    n_households.columns = [
        "Personnes seules",
        "Familles monoparentales",
        "Couples sans enfant",
        "Couples avec enfants",
        "Autres ménages"
    ]

    n_households = n_households.transpose().reset_index()
    n_households.columns = ["household_type", "n_households"]


    # Total budget
    budget_type = pd.merge(budget_type, n_households, on="household_type")
    budget_type["total_value_by_hh_type"] = budget_type["value"]*budget_type["n_households"]*1000




    # --------------------------------
    # Load household budget given the available income per consumption unit of the household
    budget_income = pd.read_excel(
        data_folder_path / "input/insee/budget/irsocbdf11_TM106.xls",
        skiprows=2,
        usecols=[0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    )

    budget_income.columns = ["budget_item_id"] + ["D" + str(i) for i in range(1, 11)]

    # Filter out grouped items
    budget_income["budget_item_id"] = budget_income["budget_item_id"].str.split(" - ", 0).str.get(0)
    budget_income = budget_income[budget_income["budget_item_id"].str.len() == 5]

    # Melt dataframe
    budget_income = pd.melt(budget_income, id_vars="budget_item_id", var_name="decile")

    # Households number
    n_households = pd.read_excel(
        data_folder_path / "input/insee/budget/irsocbdf11_cad01_fm.xls",
        skiprows=23,
        nrows=1,
        usecols=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1]
    )

    n_households.columns = ["D" + str(i) for i in range(1, 11)]

    n_households = n_households.transpose().reset_index()
    n_households.columns = ["decile", "n_households"]


    # Total budget
    budget_income = pd.merge(budget_income, n_households, on="decile")
    budget_income["total_value_by_income"] = budget_income["value"]*budget_income["n_households"]*1000



    # --------------------------------
    # Load household budget given the age of the reference adult in the household
    budget_age = pd.read_excel(
        data_folder_path / "input/insee/budget/irsocbdf11_TM102.xls",
        skiprows=2,
        usecols=[0, 2, 3, 4, 5, 6, 7, 8, 9]
    )

    budget_age.rename(columns={"Âge de la personne de référence": "budget_item_id"}, inplace=True)

    # Filter out grouped items
    budget_age["budget_item_id"] = budget_age["budget_item_id"].str.split(" - ", 0).str.get(0)
    budget_age = budget_age[budget_age["budget_item_id"].str.len() == 5]

    # Melt dataframe
    budget_age = pd.melt(budget_age, id_vars="budget_item_id", var_name="age")

    # Households number
    n_households = pd.read_excel(
        data_folder_path / "input/insee/budget/irsocbdf11_cad01_fm.xls",
        header=7,
        nrows=1,
        usecols=[1, 2, 3, 4, 5, 6, 7, 8]
    )

    n_households = n_households.transpose().reset_index()
    n_households.columns = ["age", "n_households"]


    # Total budget
    budget_age = pd.merge(budget_age, n_households, on="age")
    budget_age["total_value_by_age"] = budget_age["value"]*budget_age["n_households"]*1000



    # --------------------------------
    # Load household budget given the age of the reference adult in the household
    budget_stocd = pd.read_excel(
        data_folder_path / "input/insee/budget/irsocbdf11_TM107.xls",
        skiprows=2,
        usecols=[0, 2, 8]
    )

    budget_stocd.rename(columns={"Statut d'occupation du logement selon le niveau de vie par quintile": "budget_item_id"}, inplace=True)

    # Filter out grouped items
    budget_stocd["budget_item_id"] = budget_stocd["budget_item_id"].str.split(" - ", 0).str.get(0)
    budget_stocd = budget_stocd[budget_stocd["budget_item_id"].str.len() == 5]

    budget_stocd.columns = ["budget_item_id", "Propriétaires", "Locataires"]

    # Melt dataframe
    budget_stocd = pd.melt(budget_stocd, id_vars="budget_item_id", var_name="stocd")

    # Households number
    n_households = pd.read_excel(
        data_folder_path / "input/insee/budget/irsocbdf11_cad01_fm.xls",
        header=28,
        nrows=1,
        usecols=[1, 7]
    )

    n_households.columns = ["Propriétaires", "Locataires"]

    n_households = n_households.transpose().reset_index()
    n_households.columns = ["stocd", "n_households"]


    # Total budget
    budget_stocd = pd.merge(budget_stocd, n_households, on="stocd")
    budget_stocd["total_value_by_stocd"] = budget_stocd["value"]*budget_stocd["n_households"]*1000





    # --------------------
    # Remove income and housing tax
    # impôts directs = impôt sur le revenu, taxe d’habitation, contribution sociale généralisée [CSG],
    # contribution à la réduction de la dette sociale [CRDS],
    # et autres prélèvements sociaux sur les revenus du patrimoine).
    exclusion_list = ['13141', "131AZ"]

    budget_type = budget_type[~budget_type["budget_item_id"].isin(exclusion_list)]
    budget_stocd = budget_stocd[~budget_stocd["budget_item_id"].isin(exclusion_list)]
    budget_income = budget_income[~budget_income["budget_item_id"].isin(exclusion_list)]
    budget_age = budget_age[~budget_age["budget_item_id"].isin(exclusion_list)]

    # -------------------
    # Seed for the IPFN procedure
    # based on household counts
    ipfn_seed = pd.read_csv(data_folder_path / "input/elioth/age_household_type_stocd.csv", encoding="latin-1")

    def divide_by_decile(x):
        df = pd.DataFrame({
            "decile": ["D" + str(dec) for dec in range(1, 11)],
            "value": np.ones(10)*x["IPONDI"].values[0]/10
        })
        return df

    ipfn_seed = ipfn_seed.groupby(["age", "household_type", "stocd"]).apply(divide_by_decile)
    ipfn_seed.reset_index(inplace=True)
    ipfn_seed.drop("level_3", axis=1, inplace=True)

    ipfn_seed.sort_values(["stocd", "age", "decile", "household_type"], inplace=True)

    ipfn_seed["household_type"] = pd.Categorical(ipfn_seed["household_type"])
    ipfn_seed["decile"] = pd.Categorical(ipfn_seed["decile"])
    ipfn_seed["age"] = pd.Categorical(ipfn_seed["age"])
    ipfn_seed["stocd"] = pd.Categorical(ipfn_seed["stocd"])

    ipfn_seed["household_type_index"] = ipfn_seed["household_type"].cat.codes
    ipfn_seed["decile_index"] = ipfn_seed["decile"].cat.codes
    ipfn_seed["age_index"] = ipfn_seed["age"].cat.codes
    ipfn_seed["stocd_index"] = ipfn_seed["stocd"].cat.codes

    ipfn_seed_array = sparse.COO(
        (ipfn_seed["value"],
         (ipfn_seed["household_type_index"], ipfn_seed["decile_index"], ipfn_seed["age_index"], ipfn_seed["stocd_index"])),
        shape=(5, 10, 7, 2)
    ).todense()

    base_df = ipfn_seed[["household_type", "decile", "age", "stocd"]]

    # --------------------
    # IPFP
    def apply_ipfp(item_id):
        
        # Prepare the marginal sums
        x = budget_type.loc[budget_type["budget_item_id"] == item_id].copy()
        x.sort_values("household_type", inplace=True)
        
        y = budget_income.loc[budget_income["budget_item_id"] == item_id].copy()
        y.sort_values("decile", inplace=True)
        
        z = budget_age.loc[budget_age["budget_item_id"] == item_id].copy()
        z.sort_values("age", inplace=True)
        
        zz = budget_stocd.loc[budget_stocd["budget_item_id"] == item_id].copy()
        zz.sort_values("stocd", inplace=True)
        
        aggregates = [
            x.total_value_by_hh_type.values,
            y.total_value_by_income.values,
            z.total_value_by_age.values,
            zz.total_value_by_stocd.values
        ]
        
        # Initialize the matrix values with the average value
        # m = np.ones((x.shape[0], y.shape[0], z.shape[0]))*y.total_value_by_income.sum()/y.n_households.sum()*2780/x.shape[0]
        m = ipfn_seed_array*y.total_value_by_income.sum()/y.n_households.sum()*2780/x.shape[0]
        
        # Run the procedure
        IPF = ipfn.ipfn(m, aggregates, [[0], [1], [2], [3]])
        m = IPF.iteration()
        
        df = base_df.copy()
        df["value"] = m.ravel(order="F")
        df["budget_item_id"] = item_id
        df.set_index(["household_type", "decile", "age", "stocd"], inplace=True)
        
        return df

    # Apply the IPFP procedure for each item
    dfs = []
    for item_id in budget_income["budget_item_id"].unique():
        df = apply_ipfp(item_id)
        dfs.append(df)

    dfs = pd.concat(dfs)

    # Compute the share of each item in the total budget
    dfs["total"] = dfs.groupby(["household_type", "decile", "age", "stocd"])["value"].sum()
    dfs["share"] = dfs["value"]/dfs["total"]
    dfs.reset_index(inplace=True)

    # ---------------------------------
    # Save the result
    df_share = dfs.copy()
    df_share.drop(["value", "total"], axis="columns", inplace=True)

    output = data_folder_path / "output/budget.parquet"
    with open(output, "wb") as f:
        df_share.to_parquet(f, index=False)



if __name__ == "__main__":
    main()