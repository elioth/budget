import pandas as pd
import numpy as np
from pathlib import Path
from scipy import stats, optimize
from scipy import interpolate
import os

def load_sheet(filepath, variable, level=None):
    
    if level is None:
        sheet_name = variable
        usecols = usecols=[0, 6, 9, 10, 11, 12, 13, 14, 15, 16]
    else:
        sheet_name = variable+"_"+level
        usecols = usecols=[0, 3, 6, 7, 8, 9, 10, 11, 12, 13]
        
    df = pd.read_excel(
        filepath,
        sheet_name=sheet_name,
        header=5,
        usecols=usecols
    )
    
    if level is None:
        df.rename(columns={"Q217": "D517"}, inplace=True)
    else:
        median_column_name = [c for c in df.columns if "Q2" in c]
        df.rename(columns={median_column_name[0]: "D517"}, inplace=True)
    
    return df

def load_variable(filepath, variable, levels=None):
    if levels is None:
        df = load_sheet(filepath, variable, None)
        df = df.melt("CODGEO")
        df["level"] = "0"
        df["decile"] = df["variable"].str[-4:]
        df["decile"] = df["decile"].str.replace("17", "")
        df["variable"] = "ENSEMBLE"
    else:
        dfs = []
        for level in levels:
            df = load_sheet(filepath, variable, level)
            df = df.melt("CODGEO")
            df["level"] = level
            df["decile"] = df["variable"].str[-4:]
            df["decile"] = df["decile"].str.replace("17", "")
            dfs.append(df)
        df = pd.concat(dfs)
        df["variable"] = variable
    return df

def add_d0_d10(df):
    df_c = df.pivot_table(index=["CODGEO", "variable", "level"], columns="decile", values="value")
    df_c["D0"] = df_c["D1"] - np.maximum(0.0, 2*(df_c["D2"] - df_c["D1"]))
    df_c["D10"] = df_c["D9"] + 2*(df_c["D9"] - df_c["D8"])
    df_c.reset_index(inplace=True)
    df = df_c.melt(["CODGEO", "variable", "level"])
    return df


decile_to_x = pd.DataFrame({
    "decile": ["D" + str(i) for i in range(0, 11)],
    "x": np.arange(0, 1.1, 0.1)
})

def interp(x, y):
    f = lambda x, a, scale: stats.gamma(a=a, scale=scale).cdf(x)
    a, scale = optimize.curve_fit(f, x, y, p0=[20, 1000])[0]
    x_new = np.arange(0, 121000, 1000)
    p = stats.gamma(a=a, scale=scale).cdf(x_new)
    return pd.DataFrame({"x": x_new, "value": p})

def prepare_proba_table(filepath, variable, levels=None):

    print("Loading income distributions for the variable :", variable)
    
    df = load_variable(filepath, variable, levels)
    df = pd.merge(df, decile_to_x, on="decile")
    
    # Interpolate the cumulative distribution function
    df = df.groupby(["CODGEO", "variable", "level"]).apply(lambda x: interp(x["value"], x["x"]))
    df.index = df.index.droplevel(level=None)

    # Compute and normalize the distribution function
    df["x_lag"] = df.groupby(["CODGEO", "variable", "level"])["x"].shift(fill_value=0)
    df["value_lag"] = df.groupby(["CODGEO", "variable", "level"])["value"].shift(fill_value=0)
    df["d_value"] = df["value"] - df["value_lag"]
    df["interval"] = df["x_lag"].astype(str) + " - " + df["x"].astype(str)
    df["d_value"] = df["d_value"]/df.groupby(["CODGEO", "variable", "level"])["d_value"].sum()
    
    df = df[df["interval"] != "0 - 0"]
    
    return df

def cartesian_product(d):
    index = pd.MultiIndex.from_product(d.values(), names=d.keys())
    return pd.DataFrame(index=index).reset_index()


def main():

    data_folder_path = Path(os.path.abspath(__file__))
    data_folder_path = data_folder_path.parent / "data"

    filepath = data_folder_path / "input/insee/filosofi/FILO2017_DISP_DEP.xlsx"

    ens = prepare_proba_table(filepath, "ENSEMBLE")
    tragerf = prepare_proba_table(filepath, "TRAGERF", ["1", "2", "3", "4", "5", "6"])
    typmenr = prepare_proba_table(filepath, "TYPMENR", ["1", "2", "3", "4", "5", "6"])
    occtypr = prepare_proba_table(filepath, "OCCTYPR", ["1", "21", "22"])

    ens.reset_index(inplace=True)
    tragerf.reset_index(inplace=True)
    typmenr.reset_index(inplace=True)
    occtypr.reset_index(inplace=True)

    ens = ens[["CODGEO", "x", "d_value"]]
    ens.columns = ["DEPT", "x", "p_income"]

    tragerf = tragerf[["CODGEO", "level", "x", "d_value"]]
    tragerf.columns = ["DEPT", "tragerf", "x", "p_income_tragerf"]
       
    typmenr = typmenr[["CODGEO", "level", "x", "d_value"]]
    typmenr.columns = ["DEPT", "typmenr", "x", "p_income_typmenr"]

    occtypr = occtypr[["CODGEO", "level", "x", "d_value"]]
    occtypr.columns = ["DEPT", "occtypr", "x", "p_income_occtypr"]


    print("Computing socio-economic marginal probabilities")

    age_typ = pd.read_csv(data_folder_path / "input/elioth/age_household_type_occtypr_filosofi.csv", encoding="latin-1", dtype=str)
    age_typ = age_typ[["DEPT", "age_filosofi_v", "household_type_filosofi_v", "occtypr", "p"]].copy()
    age_typ["occtypr"] = age_typ["occtypr"].astype(float).astype(int).astype(str)
    age_typ["p"] = age_typ["p"].astype(float)
    age_typ.columns = ["DEPT", "tragerf", "typmenr", "occtypr", "p_tragerf_typmenr_occtypr"]


    tragerf_marg = age_typ.groupby(["DEPT", "tragerf"], as_index=False)["p_tragerf_typmenr_occtypr"].sum()
    typmenr_marg = age_typ.groupby(["DEPT", "typmenr"], as_index=False)["p_tragerf_typmenr_occtypr"].sum()
    occtypr_marg = age_typ.groupby(["DEPT", "occtypr"], as_index=False)["p_tragerf_typmenr_occtypr"].sum()

    tragerf_marg.set_index("DEPT", inplace=True)
    tragerf_marg["p_tragerf"] = tragerf_marg["p_tragerf_typmenr_occtypr"]/tragerf_marg.groupby(["DEPT"])["p_tragerf_typmenr_occtypr"].sum()
    tragerf_marg.reset_index(inplace=True)

    typmenr_marg.set_index("DEPT", inplace=True)
    typmenr_marg["p_typmenr"] = typmenr_marg["p_tragerf_typmenr_occtypr"]/typmenr_marg.groupby(["DEPT"])["p_tragerf_typmenr_occtypr"].sum()
    typmenr_marg.reset_index(inplace=True)

    occtypr_marg.set_index("DEPT", inplace=True)
    occtypr_marg["p_occtypr"] = occtypr_marg["p_tragerf_typmenr_occtypr"]/occtypr_marg.groupby(["DEPT"])["p_tragerf_typmenr_occtypr"].sum()
    occtypr_marg.reset_index(inplace=True)

    tragerf_marg = tragerf_marg[["DEPT", "tragerf", "p_tragerf"]]
    typmenr_marg = typmenr_marg[["DEPT", "typmenr", "p_typmenr"]]
    occtypr_marg = occtypr_marg[["DEPT", "occtypr", "p_occtypr"]]

    df = cartesian_product({
        "DEPT": age_typ["DEPT"].unique(),
        "tragerf": ["1", "2", "3", "4", "5", "6"],
        "typmenr": ["1", "2", "3", "4", "5", "6"],
        "occtypr": ["1", "21", "22"],
        "x": np.arange(0, 120000, 1000)
    })

    print("Computing income levels probabilities given socio-economic variables")

    df = pd.merge(df, ens, on=["DEPT", "x"])

    df = pd.merge(df, tragerf_marg, on=["DEPT", "tragerf"])
    df = pd.merge(df, typmenr_marg, on=["DEPT", "typmenr"])
    df = pd.merge(df, occtypr_marg, on=["DEPT", "occtypr"])

    df = pd.merge(df, tragerf, on=["DEPT", "tragerf", "x"])
    df = pd.merge(df, typmenr, on=["DEPT", "typmenr", "x"])
    df = pd.merge(df, occtypr, on=["DEPT", "occtypr", "x"])

    df = pd.merge(df, age_typ, on=["DEPT", "tragerf", "typmenr", "occtypr"])

    df["p"] = df["p_income_tragerf"]*df["p_income_typmenr"]*df["p_income_occtypr"]
    df["p"] *= df["p_tragerf"]*df["p_typmenr"]*df["p_occtypr"]
    df["p"] /= np.power(df["p_income"], 2)
    df["p"] /= df["p_tragerf_typmenr_occtypr"]

    df.fillna(0.0, inplace=True)

    df.loc[(~np.isfinite(df["p"])), "p"] = 0.0

    df.set_index(["DEPT", "tragerf", "typmenr", "occtypr", "x"], inplace=True)

    df["p"] = df["p"]/df.groupby(["DEPT", "tragerf", "typmenr", "occtypr"])["p"].sum()

    df.reset_index(inplace=True)
    df.set_index(["DEPT", "tragerf", "typmenr", "occtypr"], inplace=True)


    df["income"] = df["x"] - 500
    df.reset_index(inplace=True)

    df = df[["DEPT", "tragerf", "typmenr", "occtypr", "income", "p"]]

    print("Map Filosofi variables to BDF variables")

    sfm_household_type_filosofi = pd.read_excel(data_folder_path / "input/elioth/sfm_household_type_filosofi.xlsx", dtype=str)
    sfm_household_type_filosofi = sfm_household_type_filosofi[["household_type_filosofi_v", "household_type_filosofi"]].drop_duplicates()
    sfm_household_type_filosofi.columns = ["typmenr", "household_type_filosofi"]

    agerev_age_filosofi = pd.read_excel(data_folder_path / "input/elioth/agerev_age_filosofi.xlsx", dtype=str)
    agerev_age_filosofi = agerev_age_filosofi[["age_filosofi_v", "age_filosofi"]].drop_duplicates()
    agerev_age_filosofi.columns = ["tragerf", "age_filosofi"]

    stocd = pd.read_excel(data_folder_path / "input/elioth/stocd_filosofi.xlsx", dtype=str)
    stocd.dropna(inplace=True)
    stocd["occtypr"] = stocd["occtypr"].astype(float).astype(int).astype(str)
    stocd = stocd[["occtypr", "stocd"]].drop_duplicates()
    stocd.columns = ["occtypr", "occupation_status"]

    df = pd.merge(df, sfm_household_type_filosofi, on="typmenr")
    df = pd.merge(df, agerev_age_filosofi, on="tragerf")
    df = pd.merge(df, stocd, on="occtypr")

    df = df[["DEPT", "household_type_filosofi", "age_filosofi", "occupation_status", "income", "p"]]
    df["p"] = np.round(df["p"], 6)

    output = data_folder_path / "output/income_probability.parquet"
    print("Saving to ", output)

    with open(output, "wb") as f:
        df.to_parquet(f, index=False)


if __name__ == "__main__":
    main()
